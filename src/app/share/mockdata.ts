import { Flight } from "../component/flight";

export class Mockdata {
    public static mflight: Flight[] = [
        {
            fullName:"Job susan",
            from:"กรุงเทพ,ไทย",
            to:"พัทยา,ไทย",
            type:"Return",
            adults:1,
            departure:new Date(),
            children:2,
            infants:0,
            arrival:new Date()
        }
    ]
}
