import { Injectable } from '@angular/core';
import { Flight } from '../component/flight';
import { Mockdata } from './mockdata';

@Injectable({
  providedIn: 'root'
})
export class PageserviceService {
  flight:Flight[]=[]

  constructor() {
    this.flight = Mockdata.mflight
   }
   getFlight(): Flight[]{
     return this.flight
   }
   addFlight(f:Flight):void{
     this.flight.push(f)
   }
}
