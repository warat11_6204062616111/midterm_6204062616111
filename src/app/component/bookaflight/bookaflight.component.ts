import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup,Validators } from '@angular/forms';
import { Flight } from '../flight';
import { PageserviceService } from 'src/app/share/pageservice.service';


@Component({
  selector: 'app-bookaflight',
  templateUrl: './bookaflight.component.html',
  styleUrls: ['./bookaflight.component.css']
})
export class BookaflightComponent implements OnInit {

  flight:Flight
  FlightForm! : FormGroup
  booking:Array<Flight>=[]
   check = false
   check2 = false
   checkdate = new Date()
   todayDate:Date = new Date();

  
  From=[
    {from:'กรุงเทพ,ไทย'},
    {from:'ภูเก็ต,ไทย'},
    {from:'กระบี่,ไทย'},
    {from:'หาดใหญ่,ไทย'},
    {from:'เกาะสมุย,ไทย'},
    {from:'เชียงราย,ไทย'},
    {from:'พัทยา,ไทย'},
    {from:'สุราษฎร์ธานี,ไทย'}
  ]
  To=[
    {to:'กรุงเทพ,ไทย'},
    {to:'ภูเก็ต,ไทย'},
    {to:'กระบี่,ไทย'},
    {to:'หาดใหญ่,ไทย'},
    {to:'เกาะสมุย,ไทย'},
    {to:'เชียงราย,ไทย'},
    {to:'พัทยา,ไทย'},
    {to:'สุราษฎร์ธานี,ไทย'}
  ]

  constructor(private fb:FormBuilder,private p:PageserviceService) { 
    var myDate = new Date();
    var change = new Date(new Intl.DateTimeFormat('th-TH').format(myDate))
    var test = new Intl.DateTimeFormat('th').format(myDate);
    this.flight = new Flight('','','','',0,myDate,0,0,myDate)



    console.log(test)
    console.log(myDate);
    // console.log(new Intl.DateTimeFormat('th-TH', { dateStyle: 'medium', timeStyle: 'full' }).format(myDate))
    console.log(new Date(new Intl.DateTimeFormat('th-TH').format(myDate)))
    const javaScriptRelease = Date.parse('04 Dec 1995 00:12:00 GMT');
    console.log(new Intl.DateTimeFormat('th-TH', { dateStyle: 'medium' }).format(myDate));
  }

  ngOnInit(): void {
    this.FlightForm = this.fb.group({
      fullName:['',Validators.required],
      from:['',Validators.required],
      to:['',Validators.required],
      options:['',Validators.required],
      adults:[0,[Validators.required,Validators.pattern('^(d*[0-9])$')]],
      departure:['',Validators.required],
      children:[0,[Validators.required,Validators.pattern('^(d*[0-9])$')]],
      infants:[0,[Validators.required,Validators.pattern('^(d*[0-9])$')]],
      arrival:['',Validators.required]
    })
    this.getFlight()
  }
  getFlight(){
    this.booking = this.p.getFlight()
  }

  onSubmit(f:FormGroup):void{
    this.flight.fullName = f.get('fullName')?.value
    this.flight.from = f.get('from')?.value
    this.flight.to = f.get('to')?.value
    this.flight.type = f.get('options')?.value
    this.flight.adults = f.get('adults')?.value
    this.flight.departure = f.get('departure')?.value
    this.flight.children = f.get('children')?.value
    this.flight.infants = f.get('infants')?.value
    this.flight.arrival = f.get('arrival')?.value

    let datedeparture:any = new Intl.DateTimeFormat('th-TH', { dateStyle: 'medium' }).format(this.flight.departure);
    let datearrival:any = new Intl.DateTimeFormat('th-TH', { dateStyle: 'medium' }).format(this.flight.arrival);

    if(this.flight.from===this.flight.to){
      this.check=true
      alert("ไม่สามารถไป "+this.flight.from+" to "+this.flight.to)
    }
    else if(this.flight.departure > this.flight.arrival){
      this.check=true
      alert("ไม่สามารถเลือก วันออกเดินทาง มากกว่า วันที่ถึงได้ กรุณาเลือกใหม่")
    }
    else if(this.flight.adults<1){
      alert("adults ต้องมีอย่างน้อย1คน") 
    }
    else if(this.flight.from!=this.flight.to){
      this.check=false
    }
    if(this.flight.adults>0 && this.check===false){
      let form_recode = new Flight(this.flight.fullName,this.flight.from,this.flight.to,this.flight.type,this.flight.adults,datedeparture,this.flight.children,this.flight.infants,datearrival)
      this.p.addFlight(form_recode)
    }



  }

}
